package biz.martyn.budget;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import biz.martyn.budget.fund.Fund;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = Application.class)
public class FundsControllerTest extends ControllerTest {

    
    // Get all
    
    @Test
    public void testGetFunds_whenNoToken_thenUnauthorized() throws Exception {
        mockMvc.perform(
            get("/funds")
            ).andExpect(status().isUnauthorized());
    }

    @Test
    public void testGetFunds_whenValidToken_thenOk() throws Exception {
        String accessToken = getAccessToken(USER_USERNAME, USER_PASSWORD);
        mockMvc.perform(
            get("/funds")
                .header("Authorization", "Bearer " + accessToken)
            ).andExpect(status().isOk());
    }

    
    // Get one
    
    @Test
    public void testGetFund_whenNoToken_thenUnauthorized() throws Exception {
        mockMvc.perform(
            get("/funds")
            ).andExpect(status().isUnauthorized());
    }

    @Test
    public void testGetFund_whenValidToken_thenOk() throws Exception {
        String accessToken = getAccessToken(USER_USERNAME, USER_PASSWORD);
        mockMvc.perform(
            get("/funds")
                .header("Authorization", "Bearer " + accessToken)
            ).andExpect(status().isOk());
    }


    // Create

    @Test
    public void testCreateFund_whenNoToken_thenUnauthorized() throws Exception {
        mockMvc.perform(
            post("/funds/create")
                .param("name", "Nationwide")
                .param("amount", "1000")
                .param("currency", currency.getId().toString())
            ).andExpect(status().isUnauthorized());
    }

    @Test
    public void testCreateFund_whenValidToken_thenOk() throws Exception {
        String accessToken = getAccessToken(USER_USERNAME, USER_PASSWORD);
        mockMvc.perform(
            post("/funds/create")
                .header("Authorization", "Bearer " + accessToken)
                .param("name", "Nationwide")
                .param("amount", "1000")
                .param("currency", currency.getId().toString())
            ).andExpect(status().isCreated());
    }

    @Test
    public void testCreateFund_whenMissingRequiredFields_thenError() throws Exception {
        
        String accessToken = getAccessToken(USER_USERNAME, USER_PASSWORD);
        
        mockMvc.perform(
            post("/funds/create")
                .header("Authorization", "Bearer " + accessToken)
                // .param("name", "Nationwide")
                .param("amount", "1000")
                .param("currency", currency.getId().toString())
            ).andExpect(status().isBadRequest());
        
        mockMvc.perform(
            post("/funds/create")
                .header("Authorization", "Bearer " + accessToken)
                .param("name", "Nationwide")
                .param("amount", "1000")
                // .param("currency", currency.getId().toString())
            ).andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateFund_whenFieldsInValid_thenBadRequest() throws Exception {
        
        String accessToken = getAccessToken(USER_USERNAME, USER_PASSWORD);
        
        mockMvc.perform(
            post("/funds/create")
                .header("Authorization", "Bearer " + accessToken)
                .param("name", "") // empty 
                .param("amount", "1000")
                .param("currency", currency.getId().toString())
            ).andExpect(status().isBadRequest());
    }


    // Update

    @Test
    public void testUpdateFund_whenNoToken_thenUnauthorized() throws Exception {

        fund = fundRepository.save(
            new Fund("Nationwide", (double) 100, currency, user));

        System.out.println("d3bug: fund in controller "+fund.getId());
        
        mockMvc.perform(
            put("/funds/"+fund.getId())
                .param("name", "Nationwide")
                .param("amount", "1000")
                .param("currency", currency.getId().toString())
            ).andExpect(status().isUnauthorized());
    }

    @Test
    public void testUpdateFund_whenValidToken_thenOk() throws Exception {

        fund = fundRepository.save(
            new Fund("Nationwide", (double) 100, currency, user));

        String accessToken = getAccessToken(USER_USERNAME, USER_PASSWORD);
        mockMvc.perform(
            put("/funds/"+fund.getId())
                .header("Authorization", "Bearer " + accessToken)
                .param("name", "Nationwide")
                .param("amount", "1000")
                .param("currency", currency.getId().toString())
            ).andExpect(status().isNoContent());
    }


    // Delete

    @Test
    public void testDeleteFund_whenNoToken_thenUnauthorized() throws Exception {

        fund = fundRepository.save(
            new Fund("Nationwide", (double) 100, currency, user));
        
        mockMvc.perform(
            delete("/funds/"+fund.getId())
            ).andExpect(status().isUnauthorized());
    }

    @Test
    public void testDeleteFund_whenValidToken_thenOk() throws Exception {

        fund = fundRepository.save(
            new Fund("Nationwide", (double) 100, currency, user));
        
        String accessToken = getAccessToken(USER_USERNAME, USER_PASSWORD);
        mockMvc.perform(
            delete("/funds/"+fund.getId())
                .header("Authorization", "Bearer " + accessToken)
            ).andExpect(status().isNoContent());
    }
    
}