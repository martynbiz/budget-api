package biz.martyn.budget;

import static org.hamcrest.Matchers.is;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import biz.martyn.budget.user.UserRepository;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = Application.class)
@Ignore
// @DataJpaTest
public class RegisterControllerTest {
 
    @Autowired
    private WebApplicationContext wac;
 
    @Autowired
    private FilterChainProxy springSecurityFilterChain;
 
    private MockMvc mockMvc;
 
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
          .addFilter(springSecurityFilterChain).build();
    }

    @Test
    public void testRegister_whenValidFields_thenOk() throws Exception {
        
        mockMvc.perform(
            post("/register")
                .param("firstName", "Martyn")
                .param("surname", "Bissett")
                .param("username", "martyn")
                .param("password", "secret")
                .param("email", "martynbissett@yahoo.co.uk")
            ).andExpect(status().isOk());
    }

    @Test
    public void testRegister_whenMissingFields_thenOk() throws Exception {
        
        mockMvc.perform(
            post("/register")
                // .param("firstName", "Martyn")
                .param("surname", "Bissett")
                .param("username", "martyn")
                .param("password", "secret")
                .param("email", "martynbissett@yahoo.co.uk")
            ).andExpect(status().is4xxClientError());
        
        mockMvc.perform(
            post("/register")
                .param("firstName", "Martyn")
                // .param("surname", "Bissett")
                .param("username", "martyn")
                .param("password", "secret")
                .param("email", "martynbissett@yahoo.co.uk")
            ).andExpect(status().is4xxClientError());
    }

    // @Test
    // public void givenToken_whenGetAllUsers_thenPasswordIgnored() throws Exception {
    //     String accessToken = obtainAccessToken("martyn", "secret");
    //     mockMvc.perform(
    //         get("/users")
    //             .header("Authorization", "Bearer " + accessToken)
    //         ).andExpect(jsonPath("$.password").doesNotExist());
    // }

    // @Test
    // public void givenToken_whenGetOneUser_thenPasswordIgnored() throws Exception {
    //     String accessToken = obtainAccessToken("martyn", "secret");
    //     mockMvc.perform(
    //         get("/users/30")
    //             .header("Authorization", "Bearer " + accessToken)
    //         ).andExpect(jsonPath("$.password").doesNotExist());
    // }
}