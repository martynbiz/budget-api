package biz.martyn.budget;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.net.URI;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import biz.martyn.budget.currency.Currency;
import biz.martyn.budget.currency.CurrencyRepository;
import biz.martyn.budget.fund.Fund;
import biz.martyn.budget.fund.FundRepository;
import biz.martyn.budget.user.User;
import biz.martyn.budget.user.UserRepository;

public class ControllerTest {
 
    @Autowired
    private WebApplicationContext wac;
 
    @Autowired
    private FilterChainProxy springSecurityFilterChain;
 
    protected MockMvc mockMvc;

    private static final String CLIENTID = "cliente";

    private static final String CLIENTPASSWORD = "password";
    
    @Autowired
    protected UserRepository userRepository;
    
    @Autowired
    protected CurrencyRepository currencyRepository;
    
    @Autowired
    protected FundRepository fundRepository;

    @Autowired 
    PasswordEncoder passwordEncoder;

    protected Currency currency;
    protected User user;
    protected Fund fund;

    protected final static String USER_USERNAME = "joe";
    protected final static String USER_PASSWORD = "secret";
 
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
            .addFilter(springSecurityFilterChain).build();
    
        currency = currencyRepository.findByName("GBP").orElse(null);
        if (currency == null) {
            currency = currencyRepository.save(new Currency("GBP", "&pound;%01.2f"));
        }
        
        user = userRepository.findByUsername("joe").orElse(null);
        if (user == null) {
            user = userRepository.save(
                new User("Joe", "Bloggs", "joe@example.com", "joe", passwordEncoder.encode("secret")));
        }

        System.out.println("d3bug: user in test: "+user.getId());
    }

    protected String getAccessToken(String username, String password) throws Exception {
  
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("client_id", CLIENTID);
        params.add("grant_type", CLIENTPASSWORD);
        params.add("username", username);
        params.add("password", password);
     
        ResultActions result = mockMvc.perform(post("/oauth/token")
            .params(params)
            .with(httpBasic(CLIENTID, CLIENTPASSWORD))
            .accept("application/json;charset=UTF-8"))
            .andExpect(status().isOk())
            .andExpect(content().contentType("application/json;charset=UTF-8"));
     
        String resultString = result.andReturn().getResponse().getContentAsString();
     
        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(resultString).get("access_token").toString();
    }
}