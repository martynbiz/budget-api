package biz.martyn.budget;

import static org.hamcrest.Matchers.is;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import biz.martyn.budget.user.UserRepository;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = Application.class)
@Ignore
// @DataJpaTest
public class UsersControllerTest {
 
    @Autowired
    private WebApplicationContext wac;
 
    @Autowired
    private FilterChainProxy springSecurityFilterChain;
 
    private MockMvc mockMvc;

    private static final String CLIENTID = "cliente";

    private static final String CLIENTPASSWORD = "password";
 
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
          .addFilter(springSecurityFilterChain).build();
    }

    private String obtainAccessToken(String username, String password) throws Exception {
  
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("client_id", CLIENTID);
        params.add("grant_type", CLIENTPASSWORD);
        params.add("username", username);
        params.add("password", password);
     
        ResultActions result = mockMvc.perform(post("/oauth/token")
            .params(params)
            .with(httpBasic(CLIENTID, CLIENTPASSWORD))
            .accept("application/json;charset=UTF-8"))
            .andExpect(status().isOk())
            .andExpect(content().contentType("application/json;charset=UTF-8"));
     
        String resultString = result.andReturn().getResponse().getContentAsString();
     
        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(resultString).get("access_token").toString();
    }

    @Test
    public void givenNoToken_whenGetAllUsers_thenUnauthorized() throws Exception {
        
        mockMvc.perform(
            get("/users")
            ).andExpect(status().isUnauthorized());
    }

    @Test
    public void givenToken_whenGetAllUsers_thenOk() throws Exception {
        String accessToken = obtainAccessToken("martyn", "secret");
        mockMvc.perform(
            get("/users")
                .header("Authorization", "Bearer " + accessToken)
            ).andExpect(status().isOk());
    }

    // @Test
    // public void givenToken_whenGetAllUsers_thenPasswordIgnored() throws Exception {
    //     String accessToken = obtainAccessToken("martyn", "secret");
    //     mockMvc.perform(
    //         get("/users")
    //             .header("Authorization", "Bearer " + accessToken)
    //         ).andExpect(jsonPath("$.password").doesNotExist());
    // }

    // @Test
    // public void givenToken_whenGetOneUser_thenPasswordIgnored() throws Exception {
    //     String accessToken = obtainAccessToken("martyn", "secret");
    //     mockMvc.perform(
    //         get("/users/30")
    //             .header("Authorization", "Bearer " + accessToken)
    //         ).andExpect(jsonPath("$.password").doesNotExist());
    // }
}