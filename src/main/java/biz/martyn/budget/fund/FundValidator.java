package biz.martyn.budget.fund;

import org.springframework.stereotype.Component;

import biz.martyn.budget.MethodArgumentNotValidException;

@Component
class FundValidator {

    public void validate(Fund fund) throws MethodArgumentNotValidException {
        
        // ensure name is provided 
        if (fund.getName() == null || fund.getName() == "") {
            throw new MethodArgumentNotValidException("Name is missing");
        }
    }
}