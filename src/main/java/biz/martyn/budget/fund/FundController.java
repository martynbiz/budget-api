package biz.martyn.budget.fund;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
// import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.FieldError;

import biz.martyn.budget.MethodArgumentNotValidException;
import biz.martyn.budget.currency.Currency;
import biz.martyn.budget.currency.CurrencyRepository;
import biz.martyn.budget.user.User;
import biz.martyn.budget.user.UserRepository;

@RestController
@RequestMapping("/funds")
public class FundController
{
    @Autowired
    private FundRepository fundRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    FundValidator fundValidator;
    
    /**
     * Get all entities 
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody Iterable<Fund> index(
        Principal principal) {
        
        User user = getUser(principal);
        
        return fundRepository.findAllByUserId(user.getId());
    }

    /**
     * Get single entity 
     */
    @RequestMapping(value = "/{id:\\d+}", method = RequestMethod.GET)
    public @ResponseBody Fund get(@PathVariable("id") int id,
        Principal principal) {
        
        User user = getUser(principal);
        
        return fundRepository.findByIdAndUserId(id, user.getId())
            .orElseThrow(() -> new EntityNotFoundException("Fund ID not found: "+id));
    }

    /**
     * Create entity
     * 
     * @param name
     * @param amount
     * @param currencyId
     * @param principal
     * @return
     * @throws MethodArgumentNotValidException
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody Fund create(@RequestParam("name") String name,
            @RequestParam(value = "amount", defaultValue = "0") Double amount,
            @RequestParam("currency") Integer currencyId, Principal principal) throws MethodArgumentNotValidException {

        User user = getUser(principal);

        // get currency 
        Currency currency = currencyRepository.findById(currencyId)
            .orElseThrow(() -> new EntityNotFoundException("Currency ID not found: "+currencyId));

        Fund fund = new Fund(name, amount, currency, user);
        
        fundValidator.validate(fund);

        // write to db 
        return fundRepository.save(fund);
    }

    /**
     * Update entity 
     * @param id
     * @param name
     * @param amount
     * @param currencyId
     * @param principal
     * @return
     */
    @RequestMapping(value = "/{id:\\d+}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody Fund update(@PathVariable("id") int id, 
        @RequestParam("name") String name, 
        @RequestParam("amount") String amount, 
        @RequestParam("currency") int currencyId,
        Principal principal) {
        
        User user = getUser(principal);

        System.out.println("d3bug: user in controller: "+user.getId());

        System.out.println("d3bug: fund {id} in controller: "+id);

        // add fund to user, this will also set user of fund 
        Fund fund = fundRepository.findByIdAndUserId(id, user.getId())
            .orElseThrow(() -> new EntityNotFoundException("Fund ID not found: "+id));

        fund.setName(name);
        fund.setAmount(Double.parseDouble(amount));
        
        // set currency 
        Currency currency = currencyRepository.findById(currencyId)
            .orElseThrow(() -> new EntityNotFoundException("Currency ID not found: "+currencyId));
        fund.setCurrency(currency);

        return fundRepository.save(fund);
    }

    /**
     * Delete entity
     * @param id
     * @param principal
     */
    @RequestMapping(value = "/{id:\\d+}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void delete(@PathVariable("id") int id,
        Principal principal) {
            
        User user = getUser(principal);

        // add fund to user, this will also set user of fund 
        Fund fund = fundRepository.findByIdAndUserId(id, user.getId())
            .orElseThrow(() -> new EntityNotFoundException("Fund ID not found: "+id));

        fundRepository.delete(fund);
    }

    /**
     * Get the authorised user 
     */ 
    private User getUser(Principal principal) {
        return userRepository.findByUsername( principal.getName() )
            .orElseThrow(() -> new EntityNotFoundException("User not found for this token"));
    }

    // @ResponseStatus(HttpStatus.BAD_REQUEST)
    // @ExceptionHandler(MethodArgumentNotValidException.class)
    // public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
    //     Map<String, String> errors = new HashMap<>();
    //     ex.getBindingResult().getAllErrors().forEach((error) -> {
    //         String fieldName = ((FieldError) error).getField();
    //         String errorMessage = error.getDefaultMessage();
    //         errors.put(fieldName, errorMessage);
    //     });
    //     return errors;
    // }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public String handleValidationExceptions(MethodArgumentNotValidException e) {
        return e.getMessage();
    }
}