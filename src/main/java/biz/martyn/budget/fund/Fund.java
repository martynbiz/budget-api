package biz.martyn.budget.fund;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import biz.martyn.budget.currency.Currency;
import biz.martyn.budget.user.User;

@Entity(name = "funds")
@SQLDelete(sql = "UPDATE funds SET deleted_at = now() WHERE id = ?")
@Where(clause = "deleted_at is null")
public class Fund implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  protected Integer id;

  @Column(name = "created_at", updatable = false, nullable = false)
  @CreationTimestamp
  protected LocalDateTime createdAt;

  @Column(name = "updated_at")
  @UpdateTimestamp
  protected LocalDateTime updatedAt;

  @Column(name = "deleted_at")
  protected LocalDateTime deletedAt;

  @Column(nullable = false)
  @NotBlank
  private String name;

  @Column(nullable = false)
  private Double amount;
  
  @ManyToOne
  @JoinColumn(name="currency_id")
  private Currency currency;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name="user_id")
  private User user;

  protected Fund() {}

  public Fund(String name, Double amount, Currency currency, User user) {
    this.name = name;
    this.amount = amount;
    this.currency = currency;
    this.user = user;

    // // we'll also add fund to user to maintain state 
    // user.addFund(this);
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Currency getCurrency() {
    return currency;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }

  public void setUser(User user) {
    this.user = user;
  }

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Fund )) return false;
        return id != null && id.equals(((Fund) o).getId());
	}
	
	@Override
    public int hashCode() {
        return 31;
    }
}