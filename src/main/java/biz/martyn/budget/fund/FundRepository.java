package biz.martyn.budget.fund;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface FundRepository extends CrudRepository<Fund, Integer> {
 
    Iterable<Fund> findAllByUserId(Long userId);
    Optional<Fund> findByIdAndUserId(int id, Long userId);
}