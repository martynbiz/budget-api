package biz.martyn.budget;

public class MethodArgumentNotValidException extends Exception {

	public MethodArgumentNotValidException(String message) {
        super(message);
	}

	private static final long serialVersionUID = -4361398928780565090L;

}