package biz.martyn.budget.user;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController
{
    @Autowired
    private UserRepository userRepository;

    @Autowired 
    PasswordEncoder passwordEncoder;
    
    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody Iterable<User> index() {
        return userRepository.findAll();
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public @ResponseBody User create(@RequestParam("firstName") String firstName,
            @RequestParam("surname") String surname, @RequestParam("username") String username,
            @RequestParam("email") String email, @RequestParam("password") String password) {
        User user = new User(firstName, surname, email, username, passwordEncoder.encode(password));
        return userRepository.save(user);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody Optional<User> get(@PathVariable("id") int id) {
        return userRepository.findById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public @ResponseBody String update(@PathVariable("id") int id, @RequestParam("name") String name, @RequestParam("amount") String amount, @RequestParam("currencyCode") String currencyCode) {
        // Optional<User> user = userRepository.findById(id);
        // user.setName(name);
        // user.setAmount(Double.parseDouble(amount));
        // user.setCurrencyCode(currencyCode);
        // userRepository.save(user);
        return "\"OK\"";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody String delete(@PathVariable("id") int id) {
        // Optional<User> user = userRepository.findById(id);
        return "\"OK\"";
    }
}