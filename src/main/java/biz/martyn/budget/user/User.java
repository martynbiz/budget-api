package biz.martyn.budget.user;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import biz.martyn.budget.fund.Fund;

@Entity(name = "users")
@SQLDelete(sql = "UPDATE users SET deleted_at = now() WHERE id = ?")
@Where(clause = "deleted_at is null")
public class User implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8507204786382662588L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

	@Column(name = "created_at", updatable = false, nullable = false)
	@CreationTimestamp
	protected LocalDateTime createdAt;

	@Column(name = "updated_at")
	@UpdateTimestamp
	protected LocalDateTime updatedAt;
  
	@Column(name = "deleted_at")
	protected LocalDateTime deletedAt;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String surname;

    @Column(nullable = false, unique = true)
    private String email;
 
    @Column(nullable = false, unique = true)
    private String username;
 
	@Column(nullable = false)
	@JsonIgnore
    private String password;
  
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name="user_id")
	@JsonIgnore
	private List<Fund> funds;

	protected User() {}

	public User(String firstName, String surname, String email, String username, String password) {
		this.firstName = firstName;
		this.surname = surname;
		this.email = email;
		this.username = username;
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
        this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
        this.password = password;
	}
 
    // standard getters and setters

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
        this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
        this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
        this.email = email;
	}

	public List<Fund> getFunds() {
		return funds;
	}

	public void addFund(Fund fund) {
        funds.add(fund);
        fund.setUser(this);
    }
 
    public void removeFund(Fund fund) {
        funds.remove(fund);
        fund.setUser(null);
    }

	// public Fund getFund(int id) {
	// 	fundRepository.findByIdAndUserId(id)
    //         .orElseThrow(() -> new EntityNotFoundException("Fund ID not found: "+id));
	// }
}