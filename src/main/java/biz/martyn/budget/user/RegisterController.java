package biz.martyn.budget.user;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegisterController
{
    @Autowired
    private UserRepository userRepository;

    @Autowired 
    PasswordEncoder passwordEncoder;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public @ResponseBody User create(@RequestParam("firstName") String firstName, @RequestParam("surname") String surname, @RequestParam("username") String username, @RequestParam("email") String email, @RequestParam("password") String password) {
        User user = new User(firstName, surname, email, username, passwordEncoder.encode(password));
        return userRepository.save(user);
    }
}