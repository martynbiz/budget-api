package biz.martyn.budget.config;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import biz.martyn.budget.user.User;

public class JpaUserPrincipal implements UserDetails {

    /**
	 *
	 */
	private static final long serialVersionUID = -1480402973442569981L;
	private User user;

    //

    public JpaUserPrincipal(User user) {
        this.user = user;
    }

    //

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        final List<GrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"));
        return authorities;
    }

    // private final Collection<? extends GrantedAuthority> getAuthorities(Collection<Role> roles) {
    // 	List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
    // 	for (Role role: roles) {
    // 		authorities.add(new SimpleGrantedAuthority(role.getName()));
    // 		authorities.addAll(role.getPrivileges()
    // 				.stream()
    // 				.map(p -> new SimpleGrantedAuthority(p.getName()))
    // 				.collect(Collectors.toList()));
    // 	}
    //     return authorities;
    // }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    //

    public User getUser() {
        return user;
    }

}