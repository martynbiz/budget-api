package biz.martyn.budget.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter
{
	
	// @Override
	// public void configure(HttpSecurity http) throws Exception {
		
	// 	http
	// 		.authorizeRequests()
	// 		.antMatchers("/register*")
	// 		.permitAll();

	// 	// http
	// 	// 	.authorizeRequests()
	// 	// 	.antMatchers("/**")
	// 	// 	.hasRole("USER");
		
	// 	// http
	// 	// 	.authorizeRequests()
	// 	// 	.antMatchers("/register")
	// 	// 	.permitAll();

	// 	// Allows specifying which HttpServletRequest instances this HttpSecurity will be invoked on.
	// 	// requestMatchers()

	// 	// http
	// 	// 	.requestMatchers()
	// 	// 		.antMatchers("/rest2/**")
	// 	// 		.and()
	// 	// 	.authorizeRequests()
	// 	// 		.antMatchers("/rest/v1/test/hello").permitAll()
	// 	// 		.antMatchers("/rest/v1/test/**").denyAll()
	// 	// 		.and()
	// 	// 	.requestMatchers()
	// 	// 		.antMatchers("/rest/**")
	// 	// 		.and()
	// 	// 	.authorizeRequests()
	// 	// 		.antMatchers("/rest/v1/test/hello").permitAll();

	// 	// http.anonymous().disable();

	// 	// // public paths (e.g. register)
	// 	// String[] publicPaths = new String[]{"/register"};
	// 	// for(int i=0; i< publicPaths.length; i++) {

	// 	// 	String path = publicPaths[i];
			
	// 	// 	http.authorizeRequests()
	// 	// 		.antMatchers(path)
	// 	// 		.permitAll();
	// 	// }

	// 	// // user role
	// 	// String[] userPaths = new String[]{"/funds/**", "/currencies/**"};
	// 	// for(int i=0; i< userPaths.length; i++) {

	// 	// 	String path = userPaths[i];
			
	// 	// 	http.requestMatchers()
	// 	// 			.antMatchers(path)
	// 	// 		.and().authorizeRequests()
	// 	// 			.antMatchers(path)
	// 	// 			.access("hasRole('USER')");
	// 	// }

	// 	// // admin role
	// 	// String[] adminPaths = new String[]{"/users/**"};
	// 	// for(int i=0; i< adminPaths.length; i++) {

	// 	// 	String path = adminPaths[i];
			
	// 	// 	http.requestMatchers()
	// 	// 			.antMatchers(path)
	// 	// 		.and().authorizeRequests()
	// 	// 			.antMatchers(path)
	// 	// 			.access("hasRole('USER')"); // TODO change to ADMIN
	// 	// }

	// 	http.exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
		
	// }   

}

