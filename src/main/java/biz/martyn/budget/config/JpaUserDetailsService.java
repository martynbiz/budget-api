package biz.martyn.budget.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import biz.martyn.budget.user.User;
import biz.martyn.budget.user.UserRepository;

@Service
public class JpaUserDetailsService implements UserDetailsService {
 
    @Autowired
    private UserRepository userRepository;

    User user;
 
    @Override
    public UserDetails loadUserByUsername(String username) {
        if (user == null) {
            user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
        }
        return new JpaUserPrincipal(user);
    }

    public User getUser() {
        return user;
    }
} 