package biz.martyn.budget;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

@Where(clause = "deleted_at is null")
public class Model {

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  protected Long id;

  @Column(name = "created_at", updatable = false)
  @CreationTimestamp
  protected LocalDateTime createdAt;

  @Column(name = "updated_at")
  @UpdateTimestamp
  protected LocalDateTime updatedAt;

  @Column(name = "deleted_at")
  protected LocalDateTime deletedAt;

  public Long getId() {
    return id;
  }
}