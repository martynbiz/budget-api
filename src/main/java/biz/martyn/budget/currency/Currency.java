package biz.martyn.budget.currency;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

@Entity(name = "currencies")
@SQLDelete(sql = "UPDATE currencies SET deleted_at = now() WHERE id = ?")
@Where(clause = "deleted_at is null")
public class Currency implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  protected Integer id;

  @Column(unique = true, nullable = false)
  private String name;

  @Column(nullable = false)
  private String format;

  @Column(name = "created_at", updatable = false)
  @CreationTimestamp
  protected LocalDateTime createdAt;

  @Column(name = "updated_at")
  @UpdateTimestamp
  protected LocalDateTime updatedAt;

  @Column(name = "deleted_at")
  protected LocalDateTime deletedAt;

  protected Currency() {}

  public Currency(String name, String format) {
    this.name = name;
    this.format = format;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getFormat() {
    return format;
  }

  public void setFormat(final String format) {
    this.format = format;
  }
}