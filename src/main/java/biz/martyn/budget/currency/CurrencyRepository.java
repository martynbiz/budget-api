package biz.martyn.budget.currency;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface CurrencyRepository extends CrudRepository<Currency, Integer> {
    Optional<Currency> findByName(String name);
}