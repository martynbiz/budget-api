package biz.martyn.budget.currency;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/currencies")
public class CurrencyController
{
    @Autowired
    private CurrencyRepository currencyRepository;
    
    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody Iterable<Currency> index() {
        return currencyRepository.findAll();
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public @ResponseBody Currency create(@RequestParam("name") String name, @RequestParam("format") String format) {
        Currency fund = new Currency(name, format);
        return currencyRepository.save(fund);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody Optional<Currency> get(@PathVariable("id") int id) {
        return currencyRepository.findById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public @ResponseBody String update(@PathVariable("id") int id, @RequestParam("name") String name, @RequestParam("format") String format) {
        // Optional<Currency> fund = currencyRepository.findById(id);
        // fund.setName(name);
        // fund.setAmount(Double.parseDouble(amount));
        // fund.setCurrencyCode(format);
        // currencyRepository.save(fund);
        return "\"OK\"";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody String delete(@PathVariable("id") int id) {
        // Optional<Currency> fund = currencyRepository.findById(id);
        return "\"OK\"";
    }
}