# Budget API

TODO

https://www.baeldung.com/sql-logging-spring-boot
change logging level 

user.addFund( new Fund(...) ) ??

funds 
- in postman, empty name still creates mmm
- tests
- make update fields optional
- pagination 

others - currencies, users, transactions, tags, categories, 

FetchType
CascadeType



phase 2  

public @ResponseBody BudgetResponseBody create(...) {
    return BudgetResponseBody(fund); // ?
}

SO
- passwordEncoder in User?

tests, ought to mock tokenstore, jdbc - no passwords with /users and /users/{id}; no user found for token throws exception; unique email/username, get funds by user only, 
convert ids back to longs? test that new Fund(...user) does add fund to user.funds ; 
roles

Remove token on logout 
Build for different environments with maven profiles


## Getting started

This sections sets up dev and test environments

Create docker container for MySQL

```
docker run --name mysql -e MYSQL_ROOT_PASSWORD=mypass123 -p 33067:3306 -v ~/sql_dumps:/var/sql_dumps -d mysql:8.0.1
```

Setup MySQL container

```
$ docker exec -it mysql bash -l
# mysql -uroot -p

mysql> create database budget_dev;
mysql> create database budget_test;
mysql> create user 'budget_user'@'%' identified by 's3cret';
mysql> grant all on budget_dev.* to 'budget_user'@'%';
mysql> grant all on budget_test.* to 'budget_user'@'%';
mysql> use budget_dev
mysql> CREATE TABLE oauth_access_token (
    authentication_id varchar(255) NOT NULL PRIMARY KEY,
    token_id varchar(255) NOT NULL,
    token blob NOT NULL,
    user_name varchar(255) NOT NULL,
    client_id varchar(255) NOT NULL,
    authentication blob NOT NULL,
    refresh_token varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
mysql> use budget_test
mysql> CREATE TABLE oauth_refresh_token (
    token_id varchar(255) NOT NULL,
    token blob NOT NULL,
    authentication blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

Configure app 

```
$ cd budget/src/main/resources/
$ cp application-example.properties application.properties
$ cd budget/src/test/resources/
$ cp application-example.properties application.properties
```

Build and run the app 

```
$ mvn clean package
$ java -jar target/budget-api.war
```


==

Tutorials, etc 
- https://dzone.com/articles/securing-rest-services-with-oauth2-in-springboot-1
- https://blog.sqreen.com/authentication-best-practices-vue/
- https://medium.com/@erangadulshan.14/replace-inmemory-token-store-with-a-persistent-one-in-spring-boot-oauth2-c00a4c35f90f
- https://vladmihalcea.com/the-best-way-to-map-a-onetomany-association-with-jpa-and-hibernate/
- https://www.baeldung.com/spring-jpa-test-in-memory-database
- https://lmonkiewicz.com/programming/get-noticed-2017/spring-boot-rest-request-validation/

